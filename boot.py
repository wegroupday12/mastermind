from itertools import permutations as nPr
import requests as rq
import json
mm_url = "https://we6.talentsprint.com/wordle/game/"
register_url  = mm_url + "register"
register_dict = {"mode" : "Mastermind", "name": "UHA BOT"}
r = rq.post(register_url, json=register_dict)
register_with = json.dumps(register_dict)
create_url = mm_url + "create"
sess = rq.Session()
sess.post(register_url, json = register_dict)
resp = sess.post(register_url, json = register_dict)
mee = resp.json()['id']
create_dict = {'id': mee, "overwrite": True}
rc = sess.post(create_url, json = create_dict)
guess_url = mm_url + "guess"

all_words = []

def getting_feedback_from_api(word_to_be_tried: str):
    guess = {"id": mee, "guess": word_to_be_tried}
    correct = sess.post(guess_url, json = guess)
    dict_output = correct.json()
    return (dict_output['feedback'], dict_output['message']) # the feedback number

def load_file(s: str):
    return [line.strip() for line in s]

feedback, message = getting_feedback_from_api("valid") #demo word


def creating_list_of_all_words(file_path):
    with open(file_path, 'r') as file:
        for line in file:
            words = line.strip().split()
            if words:
                all_words.append(words[0])
    return all_words

file_path = '5letters.txt'  
probable_words = creating_list_of_all_words(file_path)



def check(word,feedback, message,probable_words):
    never_words = []
    feedback = int(feedback)
    if feedback == 5:
        result_word = ["".join(j) for j in nPr(word)]
        for i in result_word:
            if('win' in message):
                return f"WON, the secret word -> {i}"
            check(file, i, feedback, message)

    elif feedback == 0:
        
        for probable_word in probable_words:
            for letter in probable_word:
                if any([letter in word]):
                    never_words.append(probable_word)
        probable_words = [x for x in probable_words if x not in never_words]

        feedback, message = getting_feedback_from_api(probable_words[0])
        check(probable_words[0], feedback , message,probable_words)
        ##return any word from probable word which is not in never_words

    else:
        
        probable_words_to_remove = []
        for probable_word in probable_words:
            matches = sum(1 for a, b in zip(probable_word, word) if a == b)
            if matches == 4:
                probable_words_to_remove.append(probable_word)

        probable_words = [x for x in probable_words if x not in probable_words_to_remove]

        feedback, message = getting_feedback_from_api(probable_words[0])
        check(probable_words[0], feedback, message, probable_words)

print(check("unrig",feedback, message,probable_words))
