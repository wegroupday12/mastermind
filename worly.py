from itertools import permutations as nPr
import requests as rq
import json
mm_url = "https://we6.talentsprint.com/wordle/game/"
register_url  = mm_url + "register"
register_dict = {"mode" : "Mastermind", "name": "UHA BOT"}
r = rq.post(register_url, json=register_dict)
register_with = json.dumps(register_dict)
create_url = mm_url + "create"
sess = rq.Session()
sess.post(register_url, json = register_dict)
resp = sess.post(register_url, json = register_dict)
mee = resp.json()['id']
create_dict = {'id': mee, "overwrite": True}
rc = sess.post(create_url, json = create_dict)
guess_url = mm_url + "guess"
    

def getting_feedback_from_api(word_to_be_tried: str):
    guess = {"id": mee, "guess": word_to_be_tried}
    correct = sess.post(guess_url, json = guess)
    dict_output = correct.json()
    return (dict_output['feedback'], dict_output['message']) # the feedback number

def load_file(s: str):
    return [line.strip() for line in open(s)]

def has_repetitive_characters(word):
    return len(set(word)) != len(word)

def filter_words(words):
    return [word for word in words if not has_repetitive_characters(word)]



feedback, message = getting_feedback_from_api("valid") #demo word 

def check(word,feedback, message,probable_words):
    never_words = []
    feedback = int(feedback)
    if feedback == 5:
        result_word = ["".join(j) for j in nPr(word)]
        for i in result_word:
            feedback,message = getting_feedback_from_api(i)
            if 'win' in message:
                print('5',i)
                return #f"WON, the secret word -> {i}"
             
    elif feedback == 0:
        print(0)
        probable_words = [probable_word for probable_word in probable_words if not any(letter in word for letter in probable_word)]
#        for probable_word in probable_words:
#            if any(i not in word for i in probable_word):
#                never_words.append(probable_word)
#                  
#        probable_words = [x for x in probable_words if x not in never_words]

        feedback, message = getting_feedback_from_api(probable_words[0])
        check(probable_words[0], feedback , message,probable_words)
        ##return any word from probable word which is not in never_words
    
#    elif feedback == 4:
#        permutation_words = ["".join(j) for j in nPr(word,feedback)]
#        for i in permutation_words:
#            for j in probable_words:
#                if i in j:
#                    print(feedback, 4)
#                    feedback,message = getting_feedback_from_api(j)
#                    check(j,feedback,message,probable_words)
 
 
    else:
  
        permutation_words = ["".join(j) for j in nPr(word,feedback)]
        for i in permutation_words:
            for j in probable_words:
                if i in j:
                    #print(feedback, j)
                    feedback_given,message = getting_feedback_from_api(j)
                    print(feedback, j)
                    if feedback_given > feedback:
                        check(j,feedback_given,message,probable_words)
                        return
check('valid', feedback, message,filter_words(load_file("5letters.txt")))

